<?php
session_start();
include_once("../config.php");
//$result = mysqli_query($koneksi, "SELECT * FROM users ORDER BY nik DESC");

if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
?>
<?php
// include database connection file
include_once("../config.php");


// Check if form is submitted for user update, then redirect to homepage after update
if (isset($_POST['update'])) {

    $id = $_POST['idx'];
    $product_name = $_POST['product_Name'];
    //echo json_encode($user_nama);
    $product_color = $_POST['product_Color'];
    //echo json_encode($user_alamat);
    $product_price = $_POST['product_Price'];
    //echo json_encode($user_telepon);
    $product_size = $_POST['product_Size'];

    $foto = $_FILES['foto']['name'];
    //echo json_encode($foto);
    $tmp = $_FILES['foto']['tmp_name'];
    //echo json_encode($tmp); exit();

    $fotobaru = date('dmYHis') . $foto;

    $path = "../files/" . $fotobaru;

    // Proses upload
    if (move_uploaded_file($tmp, $path)) { // Cek apakah gambar berhasil diupload atau tidak
        // Query untuk menampilkan data siswa berdasarkan NIS yang dikirim
        $query = "SELECT * FROM product_master WHERE product_masterID='" . $id . "'";
        $sql = mysqli_query($koneksi, $query); // Eksekusi/Jalankan query dari variabel $query
        $data = mysqli_fetch_array($sql); // Ambil data dari hasil eksekusi $sql
        //echo json_encode($data); exit();
        // Cek apakah file foto sebelumnya ada di folder images
        if (is_file("../files/" . $data['foto'])) // Jika foto ada
            unlink("../files/" . $data['foto']); // Hapus file foto sebelumnya yang ada di folder images




            
// Proses ubah data ke Database
        $result = mysqli_query($koneksi, "UPDATE product_master SET product_name='$product_name',product_color='$product_color',product_price='$product_price',product_Size='$product_size',foto='$fotobaru' WHERE product_masterID=$id");

        if ($result) { // Cek jika proses simpan ke database sukses atau tidak
            // Jika Sukses, Lakukan :
            header("location: data-master.php"); // Redirect ke halaman index.php
        } else {
            // Jika Gagal, Lakukan :
            echo "Maaf, Terjadi kesalahan saat mencoba untuk menyimpan data ke database.";
            echo "<br><a href='edit-data-master.php'>Kembali Ke Form</a>";
        }
    } else {
        // Jika gambar gagal diupload, Lakukan :
        echo "Maaf, Gambar gagal untuk diupload.";
        echo "<br><a href='edit-data-master.php'>Kembali Ke Form</a>";
    }


    // update user data
    //$result = mysqli_query($koneksi, "UPDATE product_master SET product_name='$product_name',product_color='$product_color',product_price='$product_price',product_Size='$product_size' WHERE product_masterID=$id");
    //echo json_encode($result); exit();
    // Redirect to homepage to display updated user in list
    //header("Location: data-master.php");
}
?>

<?php
// Display selected user data based on id
// Getting id from url
$product_masterID = $_GET['product_masterID'];
//echo json_encode($product_masterID); exit();
// Fetech user data based on id
$result = mysqli_query($koneksi, "SELECT * FROM product_master WHERE product_masterID=$product_masterID");

while ($master_data = mysqli_fetch_array($result)) {

    $product_barcode = $master_data['product_Barcode'];
    $product_name = $master_data['product_Name'];
    $product_color = $master_data['product_Color'];
    $product_price = $master_data['product_Price'];
    $product_qty = $master_data['product_Qty'];
    $product_size = $master_data['product_Size'];
    $foto = $master_data['foto'];
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>User / Edit Data Master</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="data_in">
                            <div class="row">
                                <div class="col-sm-12">
                                    <fieldset>
                                        <legend style="">Edit Data Master</legend>
                                        <form action="" method="post" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Product Name</label>
                                                        <input type="text" class="form-control" name="product_Name" value="<?php echo $product_name; ?>" required />
                                                        <input type="hidden" name="idx" value=<?php echo $_GET['product_masterID']; ?>>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Product Color</label>
                                                        <input type="text" class="form-control" name="product_Color" value="<?php echo $product_color; ?>" required />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Product Size</label>
                                                        <select class="custom-select" name="product_Size" value="<?php echo $product_size; ?>" required >
                                                            <option value="<?php echo $product_size; ?>" selected><?php echo $product_size; ?></option>
                                                            <option value="S">S</option>
                                                            <option value="M">M</option>
                                                            <option value="L">L</option>
                                                            <option value="XL">XL</option>
                                                            <option value="ALL SIZE">ALL SIZE</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Product Price</label>
                                                        <input type="text" class="form-control" name="product_Price" value="<?php echo number_format($product_price, 0, ",", ".") ?>" placeholder="Product Price" required />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Image</label>
                                                        <img src="../files/<?php echo $foto; ?>" style="width: 100px; height: 100px; margin: 0 10px 15px 5px; border: 1px solid #c6c6c6; border-radius: 3px;" />
                                                        <input type="file" class="form-control-file" name="foto" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style="margin-top: 20px;">
                                                    <button type="submit" name="update" class="btn btn-primary">Update Data Master</button>
                                                    <button type="submit" class="btn btn-danger float-right"><a href="data-master.php">Cancel</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
