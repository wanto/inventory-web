<?php
session_start();

if (isset($_SESSION['akses'])) {
    header('location:' . $_SESSION['akses']);
    exit();
}

$error = '';
if (isset($_SESSION['error'])) {

    $error = $_SESSION['error'];

    unset($_SESSION['error']);
}
?>

<?php echo $error; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->
        
    </head>

    <body class="mybody">
        <div class="all">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="login">
                            <div class="logo_login">
                                <img class="img-fluid" src="images/login.png" />
                            </div>
                            <div class="label_login">
                                Sign In
                            </div>
                            <div class="main_login">
                                <form action="proses-login.php" method="post">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" name="user_nama" placeholder="Username" required />
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="user_password" placeholder="Password" required />
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </body>
</html>
