<?php
session_start();
error_reporting('E_ALL ^ E_NOTICE');
include_once("../config.php");
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
$result = mysqli_query($koneksi, "SELECT * FROM trout ORDER BY trout_ID DESC");
$result2 = mysqli_query($koneksi, "SELECT * FROM product_master ORDER BY product_masterID DESC");


if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <button class="btn btn-secondary" id="menu-toggle"><i class="fa fa-bars"></i></button>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span style="margin-right: 10px;">Helo, <?php echo $nama ?> !</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Setting</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="../logout.php">LogOut</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Transfer Out / Create Transfer Out</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_create">
                            <form method="post" action="" autocomplete="off">
                                <div class="data_action">
                                    <div class="form-group">
                                        <input type="text" name="cari" autofocus class="form-control form-control-lg barcode_insert" placeholder="Scan barcode in here">
                                        <input type="submit" name="cari2" value="save" style="" />
                                    </div>
                                </div>
                                <div class="form_action">
                                    <div class="lok_gudang float-left">
                                        <div class="lokasi_gudang">
                                            <label>Select Location</label>
                                            <select class="custom-select" name="troutdetail_to_lokasi">
                                                <option value="" selected>Select Location</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="tgl_buat float-right">
                                        <div class="form-group">
                                            <label>create</label>
                                            <input type="date" class="form-control" placeholder="" name="troutheader_created" />
                                        </div>
                                    </div>
                                    <div class="bersihkan"></div>
                                </div>
                                <div class="form_action">
                                    <div class="btn_submit">
                                        <button type="submit" class="btn btn-danger float-left btn_cancel"><a href="trout.php">Cancel</a></button>
                                        <input type="submit" name="submit" class="orm-control btn-success btn_simpan float-right" value="Submit" />
                                        <div class="bersihkan"></div>
                                    </div>
                                </div>
                            </form>

                            <?php
                            if ($_POST['cari2']) {
                                $no = 1; //buat urutan nomer
                                $cari = $_POST['cari'];

                                $sql = "SELECT * FROM product_master WHERE product_Barcode LIKE '%$cari%'";

                                $query = mysqli_query($conn, $sql);


                                if ($data = mysqli_fetch_array($query)) {

                                   $barcodeku = $data['product_Barcode'];
                                   
                                } else {
                                    echo "<table>";
                                    echo "<tr><td colspan='4'><center><h6><b>'$cari'</b> Tidak Ditemukan. Silahkan Periksa Kembali Keyword Anda</h6></center></td></tr>";
                                }

                                if (isset($_SESSION['barcode'])) {
                                    $barcode = $_SESSION['barcode'];
                                }
                                // End ambil data dari session
                                // Tambahkan array (hasil dari data session tadi) dengan data inputan yang baru
                                
                                $barcode[] = $barcodeku;
                                // End script tambah ke array
                                // Simpan data array yang baru ke session
                                
                                $_SESSION['barcode'] = $barcode;
                                // End script simpan ke session
                            }
                            ?>




                        </div>

                        <div class="data_in">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        
                                        <th>Barcode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    // Ambil data dari session
                                    
                                    if (isset($_SESSION['barcode'])) {
                                        $barcode = $_SESSION['barcode'];
                                    }
                                    // End script ambil data
                                    // Cetak dengan cara uraikan isi arraynya
                                    for ($i = 0; $i < count($barcode); $i++) {
                                        echo "<tr><td>";
                                        echo 1 + $i . "</td><td>" . $barcode[$i] . "</td></tr>";
                                    }
                                    // End script urai array
                                    ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <?php
        // include database connection file
        include_once("../config.php");
        require_once('../vendor/autoload.php');
        $uuid = \Ramsey\Uuid\Uuid::uuid4();


        if (isset($_POST['submit'])) {

            $troutheader_UUID = $uuid;
            //echo json_encode($troutheader_UUID);
            $trheader_number = 'TROUT123';
            //echo json_encode($trheader_number);
            $troutheader_to_lokasi = $_POST['troutdetail_to_lokasi'];
            //echo json_encode($troutheader_to_lokasi);
            $troutheader_created = $_POST['troutheader_created'];
            //echo json_encode($troutheader_created); 

            $troutheader_barcode = $barcode;
            //echo json_encode($troutheader_barcode); exit();

            include_once("../config.php");

            $query = mysqli_query($koneksi, "INSERT INTO trout_header(troutheader_UUID, troutheader_number, troutheader_barcode, troutheader_to_lokasi, troutheader_created) VALUES ('$troutheader_UUID', '$trheader_number', '$troutheader_barcode', '$troutheader_to_lokasi', '$troutheader_created' )");
            //echo json_encode($query); exit();
            echo "<meta http-equiv='refresh' content='0; url=trout.php'>";
        }
        ?>

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
