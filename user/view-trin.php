<?php
session_start();
error_reporting('E_ALL ^ E_NOTICE');
include_once("../config.php");
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
//$result = mysqli_query($koneksi, "SELECT * FROM trin ORDER BY trin_ID DESC");

$result2 = mysqli_query($koneksi, "SELECT * FROM product_master ORDER BY product_masterID DESC");
$result3 = mysqli_query($koneksi, "SELECT * FROM gudang");
$result4 = mysqli_query($koneksi, "SELECT * FROM gudang");

if( !isset($_SESSION['user']) )
{
	header('location:./../'.$_SESSION['akses']);
	exit();
}else{
	$nama = $_SESSION['user'];
}

?>



<?php
// Display selected user data based on id
// Getting id from url
$trinheader_ID = $_GET['trinheader_ID'];
//echo json_encode($trinheader_ID); exit();
// Fetech user data based on id
$result = mysqli_query($koneksi, "SELECT * FROM trin_header WHERE trinheader_ID=$trinheader_ID");
//echo json_encode($result); exit();
while ($master_data = mysqli_fetch_array($result)) {
    $trinheader_UUID = $master_data['trinheader_UUID'];
    //echo json_encode($trinheader_UUID);
    $trinheader_Number = $master_data['trinheader_Number'];
    //echo json_encode($trinheader_Number); 
    $trinheader_From = $master_data['trinheader_From'];
    //echo json_encode($trinheader_From); exit();
    $trinheader_To_lokasi = $master_data['trinheader_To_lokasi'];
    $trinheader_Status = $master_data['trinheader_Status'];
    $trinheader_Description = $master_data['trinheader_Description'];
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Transfer Out / View Transfer Out</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_create">
                            <form method="post" action="" autocomplete="off" class="myform">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="lokasi_gudang">
                                            <label>From Location</label>
                                            <select disabled class="custom-select" name="trinheader_From" value="<?php echo $trinheader_From ?>" required>
                                                <option value="<?php echo $trinheader_From ?>" selected><?php echo $trinheader_From ?></option>
                                            </select>
                                            <input type="hidden" name="idx" value=<?php echo $_GET['trinheader_ID']; ?>>
                                        </div>
                                        <div class="lokasi_gudang">
                                            <label>To Location</label>
                                            <select disabled class="custom-select" name="trinheader_To_lokasi" value="<?php echo $trinheader_To_lokasi ?>" required>
                                                <option value="<?php echo $trinheader_To_lokasi ?>" selected><?php echo $trinheader_To_lokasi ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="lokasi_gudang">
                                            <label>Status</label>
                                            <select disabled class="custom-select" name="trinheader_Status" value="<?php echo $trinheader_Status ?>" required>
                                                <option value="<?php echo $trinheader_Status ?>" selected><?php echo $trinheader_Status ?></option>
                                                <option value="posted">Posted</option>
                                                <option value="save">Save</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Description</label>
                                            <textarea disabled name="trinheader_Description" value="<?php echo $trinheader_Description ?>" class="form-control" id="exampleFormControlTextarea1" rows="3"><?php echo $trinheader_Description ?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form_action">
                                            <div class="btn_submit">
                                                <button type="submit" class="btn btn-danger float-left btn_cancel"><a href="trin.php">Back</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="data_in">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>TRIN Number</th>
                                        <th>Product Name</th>
                                        <th>Product Barcode</th>
                                        <th>From</th>
                                        <th>Rack</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    $sql = "SELECT trh.trinheader_Number, trd.trindetail_ProductName, trd.trindetail_Barcode, trd.trindetail_From, trd.trindetail_Rack FROM trin_header trh JOIN trin_detail trd ON trh.trinheader_UUID = trd.trinheader_UUID WHERE trd.trinheader_Number = '$trinheader_Number' ORDER BY trindetail_ID DESC";
                                    
                                    $result_set = mysqli_query($koneksi, $sql);
                                    //echo json_encode($result_set); exit();
                                    while ($row = mysqli_fetch_array($result_set)) {
                                        ?>
                                        <tr>	
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row['trinheader_Number'] ?></td>
                                            <td><?php echo $row['trindetail_ProductName'] ?></td>
                                            <td><?php echo $row['trindetail_Barcode'] ?></td>
                                            <td><?php echo $row['trindetail_From'] ?></td>
                                            <td><?php echo $row['trindetail_Rack'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
