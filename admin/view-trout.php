<?php
session_start();
error_reporting('E_ALL ^ E_NOTICE');
include_once("../config.php");
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
//$result = mysqli_query($koneksi, "SELECT * FROM trout ORDER BY trout_ID DESC");

$result2 = mysqli_query($koneksi, "SELECT * FROM product_master ORDER BY product_masterID DESC");
$result3 = mysqli_query($koneksi, "SELECT * FROM gudang");
$result4 = mysqli_query($koneksi, "SELECT * FROM gudang");



if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
?>



<?php
// Display selected user data based on id
// Getting id from url
$troutheader_ID = $_GET['troutheader_ID'];
//echo json_encode($troutheader_ID); exit();
// Fetech user data based on id
$result = mysqli_query($koneksi, "SELECT * FROM trout_header WHERE troutheader_ID=$troutheader_ID");

while ($master_data = mysqli_fetch_array($result)) {
    $troutheader_UUID = $master_data['troutheader_UUID'];
    $troutheader_Number = $master_data['troutheader_Number'];
    $troutheader_From = $master_data['troutheader_From'];
    $troutheader_To_lokasi = $master_data['troutheader_To_lokasi'];
    $troutheader_Status = $master_data['troutheader_Status'];
    $troutheader_Description = $master_data['troutheader_Description'];
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Transfer Out / View Transfer Out</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_create">
                            <form method="post" action="" autocomplete="off" class="myform">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="lokasi_gudang">
                                            <label>From Location</label>
                                            <select disabled class="custom-select" name="troutheader_From" value="<?php echo $troutheader_From ?>" required>
                                                <option value="<?php echo $troutheader_From ?>" selected><?php echo $troutheader_From ?></option>
                                            </select>
                                            <input type="hidden" name="idx" value=<?php echo $_GET['troutheader_ID']; ?>>
                                        </div>
                                        <div class="lokasi_gudang">
                                            <label>To Location</label>
                                            <select disabled class="custom-select" name="troutheader_To_lokasi" value="<?php echo $troutheader_To_lokasi ?>" required>
                                                <option value="<?php echo $troutheader_To_lokasi ?>" selected><?php echo $troutheader_To_lokasi ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="lokasi_gudang">
                                            <label>Status</label>
                                            <select disabled class="custom-select" name="troutheader_Status" value="<?php echo $troutheader_Status ?>" required>
                                                <option value="<?php echo $troutheader_Status ?>" selected><?php echo $troutheader_Status ?></option>
                                                <option value="posted">Posted</option>
                                                <option value="save">Save</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Description</label>
                                            <textarea disabled name="troutheader_Description" value="<?php echo $troutheader_Description ?>" class="form-control" id="exampleFormControlTextarea1" rows="3"><?php echo $troutheader_Description ?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form_action">
                                            <div class="btn_submit">
                                                <button type="submit" class="btn btn-danger float-left btn_cancel"><a href="trout.php">Back</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <div class="data_in">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>TROUT Number</th>
                                        <th>Product Name</th>
                                        <th>Product Barcode</th>
                                        <th>To Location</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    $sql = "SELECT trh.troutheader_Number, troutdetail_ProductName, troutdetail_Barcode, troutdetail_To_lokasi FROM trout_header trh JOIN trout_detail trd ON trh.troutheader_UUID = trd.troutheader_UUID WHERE trd.troutheader_Number = '$troutheader_Number' ORDER BY troutdetail_ID DESC";
                                    $result_set = mysqli_query($koneksi, $sql);

                                    while ($row = mysqli_fetch_array($result_set)) {
                                        ?>
                                        <tr>	
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row['troutheader_Number'] ?></td>
                                            <td><?php echo $row['troutdetail_ProductName'] ?></td>
                                            <td><?php echo $row['troutdetail_Barcode'] ?></td>
                                            <td><?php echo $row['troutdetail_To_lokasi'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
