<?php
session_start();
include_once("../config.php");
$result = mysqli_query($koneksi, "SELECT * FROM product_master ORDER BY product_masterID DESC");

if( !isset($_SESSION['user']) )
{
	header('location:./../'.$_SESSION['akses']);
	exit();
}else{
	$nama = $_SESSION['user'];
}

?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>
            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Data Master</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="create_in">
                            <button type="button" class="btn btn-success"><a href="create-data-master.php">create new</a></button>
                        </div>
                        <div class="data_in">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Product UUID</th>
                                        <th>Product Barcode</th>
                                        <th>Product Name</th>
                                        <th>Product Color</th>
                                        <th>Product Price</th>
                                        <th>Product Quantity</th>
                                        <th>Product Size</th>
                                        <th>Photo</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $no = 1;
                                    while ($user_data = mysqli_fetch_array($result)) {
                                        //$test = $user_data['user_nama']; 
                                        echo "<tr>";
                                        echo "<td>" . $no++ . "</td>";
                                        echo "<td>" . $user_data['product_UUID'] . "</td>";
                                        echo "<td>" . $user_data['product_Barcode'] . "</td>";
                                        echo "<td>" . $user_data['product_Name'] . "</td>";
                                        echo "<td>" . $user_data['product_Color'] . "</td>";
                                        echo "<td>" . number_format($user_data['product_Price'], 0, ",", ".") . "</td>";
                                        echo "<td>" . $user_data['product_Qty'] . "</td>";
                                        echo "<td>" . $user_data['product_Size'] . "</td>";
                                        echo "<td>" . "<img src='../files/" . $user_data['foto'] . "' width='65' height='65'>" . "</td>";
                                        echo "<td align='center'> <a href='edit-data-master.php?product_masterID=$user_data[product_masterID]' title='Edit Data' style='text-decoration: none;'><i class='fa fa-pencil'></i></a> | <a data-id='1' class='hapus' title='Delete Data' href='delete-data-master.php?product_masterID=$user_data[product_masterID]' style='text-decoration: none;' onClick=\"return confirm('Are you sure want to delete ?')\"><i class='fa fa-trash'></i></a></td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
<!--        <script>
            $(".hapus").click(function () {
                var jawab = confirm("Anda Yakin Ingin Menghapus Data Ini ?");
                if (jawab === true) {
                    // konfirmasi
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('delete-data-master.php', {id: $(this).attr('data-id')},
                        function (data) {
                            alert(data);
                        });
                        hapus = false;
                    }
                } else {
                    return false;
                }
            });
        </script>-->
      
        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
