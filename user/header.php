<?php
session_start();

$user = "localhost";
$name = "root";
$pass = "rootpassword";
$dbname = "web-inventory";

$con = mysqli_connect($user, $name, $pass, $dbname);

if (!$con) {
    die("Database Tidak Ada : " . mysqli_connect_error());
}

$queri = mysqli_query($con, "SELECT * FROM users");

$data = array();
while (($row = mysqli_fetch_array($queri)) != null) {
    $data[] = $row;
}
$cont = count($data);

$jml = "" . $cont;

//$kueri2 = mysqli_query($con, "SELECT * FROM barang_masuk");
//
//$data2 = array();
//while (($row = mysqli_fetch_array($kueri2)) != null) {
//    $data2[] = $row;
//}
//$cont2 = count($data2);
//$jml2 = "" . $cont2;
//
//
//$kueri3 = mysqli_query($con, "SELECT * FROM barang_keluar");
//
//$data3 = array();
//while (($row = mysqli_fetch_array($kueri3)) != null) {
//    $data3[] = $row;
//}
//$cont3 = count($data3);
//$jml3 = "" . $cont3;
//
//
//$kueri4 = mysqli_query($con, "SELECT * FROM gudang");
//
//$data4 = array();
//while (($row = mysqli_fetch_array($kueri4)) != null) {
//    $data4[] = $row;
//}
//$cont4 = count($data4);
//$jml4 = "" . $cont4;

if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">