<?php

// Load file koneksi.php
include_once("../config.php");

// Load plugin PHPExcel nya
require_once 'PHPExcel/PHPExcel.php';

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel
$csv->getProperties()->setCreator('My Notes Code')
        ->setLastModifiedBy('My Notes Code')
        ->setTitle("Data Inventory")
        ->setSubject("Product")
        ->setDescription("report all TRIN data")
        ->setKeywords("Data Inventory");

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Product Barcode");
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Product Name");
$csv->setActiveSheetIndex(0)->setCellValue('D1', "From");
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Rack");

// Buat query untuk menampilkan semua data siswa
$sql = mysqli_query($koneksi, "SELECT * FROM trin_detail");

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
while ($data = mysqli_fetch_array($sql)) { // Ambil semua data dari hasil eksekusi $sql
    $csv->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data['trindetail_Barcode']);
    $csv->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data['trindetail_ProductName']);
    $csv->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data['trindetail_From']);
    $csv->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data['trindetail_Rack']);

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
$csv->getActiveSheet(0)->setTitle("report all TRIN data");
$csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Inventory Location.csv"'); // Set nama file excel nya
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
