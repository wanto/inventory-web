<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading">Logo</div>
    <div class="status_login"><i class="fa fa-user-circle"></i> <?php echo $nama; ?></div>
    <div class="list-group list-group-flush">
        <ul class="list-unstyled components">
            <li>
                <a href="index.php"><i class="fa fa-tachometer"></i>Dashboard</a>
            </li>
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-files-o"></i>Transaction</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="location.php">Location</a>
                    </li>
                    <li>
                        <a href="data-master.php">Data Master</a>
                    </li>
                    <li>
                        <a href="trin.php">TRIN ( <span style="font-style: italic;">Transfer In</span> )</a>
                    </li>
                    <li>
                        <a href="trout.php">TROUT ( <span style="font-style: italic;">Transfer Out</span> )</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-files-o"></i>Report</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        <a href="trin-report.php">TRIN Report</a>
                    </li>
                    <li>
                        <a href="trout-report.php">TROUT Report</a>
                    </li>
                    <li>
                        <a href="inv-loc.php">INV Loc</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="user.php"><i class="fa fa-user-o"></i>User</a>
            </li>
            <li>
                <a href="productiontools.php"><i class="fa fa-files-o"></i>Production  Tools</a>
            </li>
        </ul>  
    </div>
</div>
<!-- /#sidebar-wrapper -->