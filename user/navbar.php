<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
    <button class="btn btn-secondary" id="menu-toggle"><i class="fa fa-bars"></i></button>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span style="margin-right: 10px; text-transform: capitalize;">Helo, <?php echo $nama ?> !</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <ul class="profile-out">
                        <li class="prof-img">
                            <img src="../images/login.png" class="rounded-circle" alt="User Image">
                            <p style="text-transform: capitalize"><?php echo $nama ?></p>
                        </li>
                        <li>
                            <a class="" href="../logout.php">Sign Out</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</nav>