<?php
session_start();
error_reporting('E_ALL ^ E_NOTICE');
include_once("../config.php");
//$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
//$dataesult = mysqli_query($koneksi, "SELECT * FROM trout ORDER BY trout_ID DESC");

if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>TRIN Report</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="bersihkan"></div>
                            </div>
                        </div>

                        <div class="pt">
                            <form method="post" action="" autocomplete="on">
                                <div class="row cari-barcode">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="labelin">
                                            search trin report
                                        </div>
                                        <div class="wrap-search">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="cari" placeholder="Look for the TRIN number here" required />
                                                <span class="input-group-btn">
                                                    <input type="submit" class="btn btn-success" name="cari-tr" value="Search" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </form>

                            <?php
                            if ($_POST['cari-tr']) {

                                $cari = $_POST['cari'];

                                $sql = "SELECT * FROM trin_header WHERE trinheader_Number = '$cari' ORDER BY trinheader_ID DESC";

                                $query = mysqli_query($koneksi, $sql);

                                if ($data = mysqli_fetch_array($query)) {
                                    ?>
                                    <div class="hhh">
                                        <table id="w" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Trin Number</th>
                                                    <th>Trout Number</th>
                                                    <th>From Location</th>
                                                    <th>To Location</th>
                                                    <th>Status</th>
                                                    <th>Description</th>
                                                    <th>Created</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php
                                                echo "<tr>";
                                                echo "<td>" . $data['trinheader_Number'] . "</td>";
                                                echo "<td>" . $data['troutheader_Number'] . "</td>";
                                                echo "<td>" . $data['trinheader_From'] . "</td>";
                                                echo "<td>" . $data['trinheader_To_lokasi'] . "</td>";
                                                echo "<td>" . $data['trinheader_Status'] . "</td>";
                                                echo "<td>" . $data['trinheader_Description'] . "</td>";
                                                echo "<td>" . $data['trinheader_created'] . "</td>";
                                                echo "<td align='center'> <a href='proses-export-trin.php?trinheader_ID=$data[trinheader_ID]' title='Export to excel' style='text-decoration: none;'>Download</a></td>";
                                                echo "</tr>";
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <?php
                                } else {
                                    echo "<table style='width: 100%;'>";
                                    echo "<tr><td colspan='4'><center><h6>Barcode ( <span style='color: red;'>$cari</span> ) Not found...!!!</h6></center></td></tr>";
                                    echo "</table>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
