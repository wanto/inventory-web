<?php
session_start();
include_once("../config.php");
$result = mysqli_query($koneksi, "SELECT * FROM users ORDER BY user_ID DESC");

if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
?>
<?php
// include database connection file
include_once("../config.php");

// Check if form is submitted for user update, then redirect to homepage after update
if (isset($_POST['update'])) {
    $id = $_POST['idx'];
    $user_password = md5($_POST['user_password']);
    //echo json_encode($level);
    // update user data
    $result = mysqli_query($koneksi, "UPDATE users SET user_password='$user_password' WHERE user_ID=$id");
    //echo json_encode($result); exit();
    // Redirect to homepage to display updated user in list
    header("Location: user.php");
}
?>

<?php
// Display selected user data based on id
// Getting id from url
$user_ID = $_GET['user_ID'];
//echo json_encode($user_ID); exit();
// Fetech user data based on id
$result = mysqli_query($koneksi, "SELECT * FROM users WHERE user_ID=$user_ID");

while ($user_data = mysqli_fetch_array($result)) {

    $user_nama = $user_data['user_nama'];
    $user_alamat = $user_data['user_alamat'];
    $user_telepon = $user_data['user_telepon'];
    $user_divisi = $user_data['user_divisi'];
    $user_level = $user_data['user_level'];
    $user_password = $user_data['user_password'];
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>User / Edit Password</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="data_in">
                            <div class="row">
                                <div class="col-sm-12">
                                    <fieldset>
                                        <legend style="">Edit Password</legend>
                                        <form action="" method="post">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <input type="text" class="form-control" name="user_nama" value="<?php echo $user_nama; ?>" required disabled />
                                                        <input type="hidden" name="idx" value=<?php echo $_GET['user_ID']; ?>>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <input type="text" class="form-control" name="user_alamat" value="<?php echo $user_alamat; ?>" required disabled />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Old Password</label>
                                                        <input type="password" class="form-control" name="user_password" value="<?php echo $user_password; ?>" required disabled />
                                                    </div>
                                                    <div class="form-group">
                                                        <label>New Password</label>
                                                        <input type="text" class="form-control" name="user_password" required placeholder="input new password" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" style="margin-top: 20px;">
                                                    <button type="submit" name="update" class="btn btn-primary">Update Password</button>
                                                    <button type="submit" class="btn btn-danger float-right"><a href="user.php">Cancel</a></button>
                                                </div>
                                            </div>
                                        </form>
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
