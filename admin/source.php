<?php

// Set header type konten.
header("Content-Type: application/json; charset=UTF-8");

// Deklarasi variable untuk koneksi ke database.
$host = "localhost";    // Server database
$username = "root";         // Username database
$password = "rootpassword";             // Password database
$database = "web-inventory"; // Nama database
// Koneksi ke database.
$conn = new mysqli($host, $username, $password, $database);

// Deklarasi variable keyword buah.
$troutnumber = $_GET["query"];

// Query ke database.
$query = $conn->query("SELECT * FROM trout_header WHERE troutheader_Number LIKE '%$troutnumber%' ORDER BY troutheader_ID DESC");
$result = $query->fetch_all(MYSQLI_ASSOC);

// Format bentuk data untuk autocomplete.
foreach ($result as $data) {
    $output['suggestions'][] = [
        'value' => $data['troutheader_Number'],
        'troutheader_Number' => $data['troutheader_Number']
    ];
}

if (!empty($output)) {
    // Encode ke format JSON.
    echo json_encode($output);
}