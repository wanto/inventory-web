<?php
session_start();

include_once("../config.php");

$kueri = mysqli_query($koneksi, "SELECT * FROM trin_header");

$data = array();
while (($row = mysqli_fetch_array($kueri)) != null) {
    $data[] = $row;
}
$cont = count($data);
$jml = "" . $cont;

$kueri2 = mysqli_query($koneksi, "SELECT * FROM trout_header");

$data2 = array();
while (($row = mysqli_fetch_array($kueri2)) != null) {
    $data2[] = $row;
}
$cont2 = count($data2);
$jml2 = "" . $cont2;


$kueri3 = mysqli_query($koneksi, "SELECT * FROM users");

$data3 = array();
while (($row = mysqli_fetch_array($kueri3)) != null) {
    $data3[] = $row;
}
$cont3 = count($data3);
$jml3 = "" . $cont3;



if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="all-info">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="transaksi">
                                        <div class="transaksi-content">
                                            <div class="img-font">
                                                <i class="fa fa-download"></i>
                                            </div>
                                            <div class="title-info">
                                                <div class="ttl_chil">
                                                    TRIN ( Transfer In )
                                                </div>
                                                <div class="ttl_chil">
                                                    <?php echo $jml; ?>
                                                </div>
                                            </div>
                                            <div class="bersihkan"></div>
                                        </div>
                                        <div class="transaksi-action">
                                            <span><i class="fa fa-eye"></i></span>
                                            <span class="lihat"><a href="trin.php">view</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="transaksi">
                                        <div class="transaksi-content">
                                            <div class="img-font">
                                                <i class="fa fa-upload"></i>
                                            </div>
                                            <div class="title-info">
                                                <div class="ttl_chil">
                                                    TROUT ( Transfer Out )
                                                </div>
                                                <div class="ttl_chil">
                                                    <?php echo $jml2; ?>
                                                </div>
                                            </div>
                                            <div class="bersihkan"></div>
                                        </div>
                                        <div class="transaksi-action">
                                            <span><i class="fa fa-eye"></i></span>
                                            <span class="lihat"><a href="trout.php">View</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="transaksi">
                                        <div class="transaksi-content">
                                            <div class="img-font">
<!--                                                <i class="fa fa-archive"></i>-->
                                                <i class="fa fa-user-o"></i>
                                            </div>
                                            <div class="title-info">
                                                <div class="ttl_chil">
                                                    Users
                                                </div>
                                                <div class="ttl_chil">
                                                    <?php echo $jml3; ?>
                                                </div>
                                            </div>
                                            <div class="bersihkan"></div>
                                        </div>
                                        <div class="transaksi-action">
                                            <span><i class="fa fa-eye"></i></span>
                                            <span class="lihat"><a href="user.php">View</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="produk_home">
                            <div class="row">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    </body>
</html>
