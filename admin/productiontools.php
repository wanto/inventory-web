<?php
session_start();
error_reporting('E_ALL ^ E_NOTICE');
include_once("../config.php");
//$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
//$dataesult = mysqli_query($koneksi, "SELECT * FROM trout ORDER BY trout_ID DESC");

if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Production Tools</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="bersihkan"></div>
                            </div>
                        </div>

                        <div class="pt">
                            <form method="post" action="" autocomplete="off">
                                <div class="row cari-barcode">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="labelin">
                                                    find item details
                                                </div>
                                                <div class="wrap-search">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="cari" placeholder="Search barcode in here" required />
                                                        <span class="input-group-btn">
                                                            <input type="submit" class="btn btn-success" name="cari-barang" value="Search" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </form>

                            <?php
                            if ($_POST['cari-barang']) {

                                $cari = $_POST['cari'];

                                $sql = "SELECT * FROM product_item_mstr WHERE product_Barcode = '$cari'";

                                $query = mysqli_query($koneksi, $sql);

                                if ($data = mysqli_fetch_array($query)) {
                                    ?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <fieldset>
                                                <legend>Products Details</legend>
                                                <div class="row main-product">
                                                    <div class="col-md-6">
                                                        <div class="product_detail">
                                                            <table>
                                                                <tr>
                                                                    <td class="input_name">
                                                                        <p class="jenis_name">Product Barcode</p>
                                                                        <input type="text" readonly value="<?php echo $data['product_Barcode']; ?>" class="input_text" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="input_name">
                                                                        <p class="jenis_name">Product Name</p>
                                                                        <input type="text" readonly value="<?php echo $data['product_Name']; ?>" class="input_text" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="input_name">
                                                                        <p class="jenis_name">Product Color</p>
                                                                        <input type="text" readonly value="<?php echo $data['product_Color']; ?>" class="input_text" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="input_name">
                                                                        <p class="jenis_name">Product Price</p>
                                                                        <input type="text" readonly value="<?php echo number_format($data['product_Price'], 0, ",", "."); ?>" class="input_text" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="input_name">
                                                                        <p class="jenis_name">Product Size</p>
                                                                        <input type="text" readonly value="<?php echo $data['product_Size']; ?>" class="input_text" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="product_img">
                                                            <?php echo "<img class='img-fluid' src='../files/" . $data['foto'] . "' />" ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                    <?php
                                } else {
                                    echo "<table style='width: 100%;'>";
                                    echo "<tr><td colspan='4'><center><h6>Barcode ( <span style='color: red;'>$cari</span> ) Not found...!!!</h6></center></td></tr>";
                                    echo "</table>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
