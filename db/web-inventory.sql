-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Jul 2019 pada 17.06
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web-inventory`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barcode_trin_temp`
--

CREATE TABLE `barcode_trin_temp` (
  `id` int(11) NOT NULL,
  `trinheader_UUID` varchar(100) NOT NULL,
  `trinheader_Number` varchar(50) NOT NULL,
  `trindetail_Barcode` varchar(100) NOT NULL,
  `trindetail_ProductName` varchar(50) NOT NULL,
  `trindetail_From` varchar(15) NOT NULL,
  `trindetail_Rack` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barcode_trin_temp`
--

INSERT INTO `barcode_trin_temp` (`id`, `trinheader_UUID`, `trinheader_Number`, `trindetail_Barcode`, `trindetail_ProductName`, `trindetail_From`, `trindetail_Rack`) VALUES
(12, '66a04f6d-4497-4e7a-bb69-8f9a585abc41', 'TRIN-2019-07-00001', '567890', 'baju dua', 'gudang B', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gudang`
--

CREATE TABLE `gudang` (
  `id_gudang` int(11) NOT NULL,
  `nama_gudang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gudang`
--

INSERT INTO `gudang` (`id_gudang`, `nama_gudang`) VALUES
(1, 'gudang A'),
(2, 'gudang B'),
(4, 'gudang c');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_item_mstr`
--

CREATE TABLE `product_item_mstr` (
  `product_masterID` int(11) NOT NULL,
  `product_UUID` varchar(100) NOT NULL,
  `product_Barcode` varchar(100) NOT NULL,
  `product_Name` varchar(50) NOT NULL,
  `product_Color` varchar(50) NOT NULL,
  `product_Price` varchar(25) NOT NULL,
  `product_Size` varchar(25) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_item_mstr`
--

INSERT INTO `product_item_mstr` (`product_masterID`, `product_UUID`, `product_Barcode`, `product_Name`, `product_Color`, `product_Price`, `product_Size`, `foto`) VALUES
(17, '74210ed3-7720-4c20-93f0-8047b79a5176', '987654', 'baju satu', 'red', '155000', 'M', '15072019061110polo1.jpg'),
(18, 'd575d144-9b65-40bc-8ddb-e942c5f31ef9', '567890', 'baju dua', 'black', '177000', 'M', '15072019061256index.jpg'),
(19, '69e7560f-2fdb-4019-bd05-1b66f6ea1726', '12345', 'hfdh', 'gjg', '363667', 'S', '21072019054355polo1.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_master`
--

CREATE TABLE `product_master` (
  `product_masterID` int(11) NOT NULL,
  `product_UUID` varchar(100) NOT NULL,
  `product_Barcode` varchar(100) NOT NULL,
  `product_Name` varchar(50) NOT NULL,
  `product_Color` varchar(50) NOT NULL,
  `product_Price` varchar(25) NOT NULL,
  `product_Qty` int(11) NOT NULL,
  `product_Size` varchar(25) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product_master`
--

INSERT INTO `product_master` (`product_masterID`, `product_UUID`, `product_Barcode`, `product_Name`, `product_Color`, `product_Price`, `product_Qty`, `product_Size`, `foto`) VALUES
(1, '69e7560f-2fdb-4019-bd05-1b66f6ea1726', '12345', 'hfdh', 'gjg', '363667', 1, 'S', '21072019054355polo1.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trin_detail`
--

CREATE TABLE `trin_detail` (
  `trindetail_ID` int(11) NOT NULL,
  `trinheader_UUID` varchar(100) NOT NULL,
  `trinheader_Number` varchar(50) NOT NULL,
  `trindetail_Barcode` varchar(100) NOT NULL,
  `trindetail_ProductName` varchar(50) NOT NULL,
  `trindetail_From` varchar(15) NOT NULL,
  `trindetail_Rack` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trin_header`
--

CREATE TABLE `trin_header` (
  `trinheader_ID` int(11) NOT NULL,
  `trinheader_UUID` varchar(100) NOT NULL,
  `trinheader_Number` varchar(50) NOT NULL,
  `troutheader_Number` varchar(50) NOT NULL,
  `trinheader_From` varchar(25) NOT NULL,
  `trinheader_To_lokasi` varchar(25) NOT NULL,
  `trinheader_Status` varchar(25) NOT NULL,
  `trinheader_Description` varchar(255) NOT NULL,
  `trinheader_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trin_header`
--

INSERT INTO `trin_header` (`trinheader_ID`, `trinheader_UUID`, `trinheader_Number`, `troutheader_Number`, `trinheader_From`, `trinheader_To_lokasi`, `trinheader_Status`, `trinheader_Description`, `trinheader_created`) VALUES
(2, '66a04f6d-4497-4e7a-bb69-8f9a585abc41', 'TRIN-2019-07-00001', 'TROUT-2019-07-00001', 'gudang B', 'gudang A', 'save', 'TRIN FROM GUDANG b -> GUDANG A', '2019-07-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trout_detail`
--

CREATE TABLE `trout_detail` (
  `troutdetail_ID` int(11) NOT NULL,
  `troutheader_UUID` varchar(100) NOT NULL,
  `troutheader_Number` varchar(50) NOT NULL,
  `troutdetail_Barcode` varchar(100) NOT NULL,
  `troutdetail_ProductName` varchar(50) DEFAULT NULL,
  `troutdetail_To_lokasi` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trout_detail`
--

INSERT INTO `trout_detail` (`troutdetail_ID`, `troutheader_UUID`, `troutheader_Number`, `troutdetail_Barcode`, `troutdetail_ProductName`, `troutdetail_To_lokasi`) VALUES
(10, '51fc65ed-b7ad-4dce-ae99-eb7d16a9b2b6', 'TROUT-2019-07-00001', '987654', 'baju satu', 'gudang A'),
(11, '51fc65ed-b7ad-4dce-ae99-eb7d16a9b2b6', 'TROUT-2019-07-00001', '567890', 'baju dua', 'gudang A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trout_header`
--

CREATE TABLE `trout_header` (
  `troutheader_ID` int(11) NOT NULL,
  `troutheader_UUID` varchar(100) NOT NULL,
  `troutheader_Number` varchar(50) NOT NULL,
  `troutheader_From` varchar(25) NOT NULL,
  `troutheader_To_lokasi` varchar(25) NOT NULL,
  `troutheader_Status` varchar(25) NOT NULL,
  `troutheader_Description` varchar(255) NOT NULL,
  `troutheader_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trout_header`
--

INSERT INTO `trout_header` (`troutheader_ID`, `troutheader_UUID`, `troutheader_Number`, `troutheader_From`, `troutheader_To_lokasi`, `troutheader_Status`, `troutheader_Description`, `troutheader_created`) VALUES
(2, '51fc65ed-b7ad-4dce-ae99-eb7d16a9b2b6', 'TROUT-2019-07-00001', 'gudang B', 'gudang A', 'save', 'TROUT FROM GUDANG B -> GUDANG A', '2019-07-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_ID` int(11) UNSIGNED NOT NULL,
  `user_UUID` varchar(50) NOT NULL,
  `user_nama` varchar(25) NOT NULL,
  `user_alamat` varchar(50) NOT NULL,
  `user_telepon` varchar(50) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_level` enum('admin','user') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_ID`, `user_UUID`, `user_nama`, `user_alamat`, `user_telepon`, `user_password`, `user_level`) VALUES
(1, '0000782f-139c-11e7-a02d-6cae8b5cac01', 'admin', 'jakarta', '02197018590', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(9, 'db02b0a5-0fda-48e0-b33e-2a369a6b8828', 'andi', 'cilacap', '021999888', 'ce0e5bf55e4f71749eade7a8b95c4e46', 'user');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barcode_trin_temp`
--
ALTER TABLE `barcode_trin_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id_gudang`);

--
-- Indeks untuk tabel `product_item_mstr`
--
ALTER TABLE `product_item_mstr`
  ADD PRIMARY KEY (`product_masterID`);

--
-- Indeks untuk tabel `product_master`
--
ALTER TABLE `product_master`
  ADD PRIMARY KEY (`product_masterID`);

--
-- Indeks untuk tabel `trin_detail`
--
ALTER TABLE `trin_detail`
  ADD PRIMARY KEY (`trindetail_ID`);

--
-- Indeks untuk tabel `trin_header`
--
ALTER TABLE `trin_header`
  ADD PRIMARY KEY (`trinheader_ID`);

--
-- Indeks untuk tabel `trout_detail`
--
ALTER TABLE `trout_detail`
  ADD PRIMARY KEY (`troutdetail_ID`);

--
-- Indeks untuk tabel `trout_header`
--
ALTER TABLE `trout_header`
  ADD PRIMARY KEY (`troutheader_ID`,`troutheader_UUID`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_ID`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barcode_trin_temp`
--
ALTER TABLE `barcode_trin_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id_gudang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `product_item_mstr`
--
ALTER TABLE `product_item_mstr`
  MODIFY `product_masterID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `product_master`
--
ALTER TABLE `product_master`
  MODIFY `product_masterID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `trin_detail`
--
ALTER TABLE `trin_detail`
  MODIFY `trindetail_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trin_header`
--
ALTER TABLE `trin_header`
  MODIFY `trinheader_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `trout_detail`
--
ALTER TABLE `trout_detail`
  MODIFY `troutdetail_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `trout_header`
--
ALTER TABLE `trout_header`
  MODIFY `troutheader_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `user_ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
