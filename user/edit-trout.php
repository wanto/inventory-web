<?php
session_start();
error_reporting('E_ALL ^ E_NOTICE');
include_once("../config.php");
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
//$result = mysqli_query($koneksi, "SELECT * FROM trout ORDER BY trout_ID DESC");

$result2 = mysqli_query($koneksi, "SELECT * FROM product_master ORDER BY product_masterID DESC");
$result3 = mysqli_query($koneksi, "SELECT * FROM gudang");
$result4 = mysqli_query($koneksi, "SELECT * FROM gudang");

if( !isset($_SESSION['user']) )
{
	header('location:./../'.$_SESSION['akses']);
	exit();
}else{
	$nama = $_SESSION['user'];
}

?>

<?php
// Display selected user data based on id
// Getting id from url
$troutheader_ID = $_GET['troutheader_ID'];
//echo json_encode($troutheader_ID); exit();
// Fetech user data based on id
$result = mysqli_query($koneksi, "SELECT * FROM trout_header WHERE troutheader_ID=$troutheader_ID");

while ($master_data = mysqli_fetch_array($result)) {
    $troutheader_UUID = $master_data['troutheader_UUID'];
    $troutheader_Number = $master_data['troutheader_Number'];
    $troutheader_From = $master_data['troutheader_From'];
    $troutheader_To_lokasi = $master_data['troutheader_To_lokasi'];
    $troutheader_Status = $master_data['troutheader_Status'];
    $troutheader_Description = $master_data['troutheader_Description'];
}
?>

<?php
// include database connection file
include_once("../config.php");

// Check if form is submitted for user update, then redirect to homepage after update
if (isset($_POST['update'])) {

    $id = $_POST['idx'];

    $troutheader_Number = $_POST['troutheader_Number'];

    $troutheader_From = $_POST['troutheader_From'];
    //echo json_encode($user_nama);
    $troutheader_To_lokasi = $_POST['troutheader_To_lokasi'];
    //echo json_encode($user_alamat);
    $troutheader_Status = $_POST['troutheader_Status'];
    //echo json_encode($user_telepon);
    $troutheader_Description = $_POST['troutheader_Description'];

    // update data
    $result = mysqli_query($koneksi, "UPDATE trout_header SET troutheader_From='$troutheader_From',troutheader_To_lokasi='$troutheader_To_lokasi',troutheader_Status='$troutheader_Status',troutheader_Description='$troutheader_Description' WHERE troutheader_ID=$id");
    //echo json_encode($result); exit();
//    $getbarcode = mysqli_query($koneksi, "SELECT * FROM trout_detail trd JOIN trout_header trh ON trh.troutheader_UUID = trd.troutheader_UUID WHERE trh.troutheader_Number = '$troutheader_Number'");
//    //echo json_encode($getbarcode); exit();
//
//    while ($datacode = mysqli_fetch_array($getbarcode)) {
//        $no = 1;
//        $troutdetail_ID = $datacode['troutdetail_ID'];
//        echo json_encode($troutdetail_ID); exit();
//        "<table>";
//        "<tr>";
//        "<td>" . $no++ . "</td>";
//        "<td>" . $datacode['troutdetail_Barcode'] . "</td>";
//        "<td>". "<input type='checkbox' name='pilih[]' value='$troutdetail_ID' checked />" . "</td>";
//        "</tr>";
//        "</table>";
//        
//        include_once("../config.php");
//        
//        $allbarcode = $_POST['pilih'];
//        echo json_encode($allbarcode); exit();
//        $jumlah_dipilih = count($allbarcode);
//        
//        echo json_encode($jumlah_dipilih); exit();
//    }
//
//    $querydeleteproduct = mysqli_query($koneksi, "DELETE FROM product_master WHERE product_barcode = $getbarcode");
//    echo json_encode($querydeleteproduct);
//    exit();
    // Redirect to homepage to display updated user in list
    header("Location: trout.php");
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Transfer Out / Edit Transfer Out</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_create">
                            <form method="post" action="" autocomplete="off" class="myform">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="lokasi_gudang">
                                            <input type="hidden" name="troutheader_Number" value="<?php echo $troutheader_Number ?>" />
                                            <label>From Location</label>
                                            <select class="custom-select" name="troutheader_From" value="<?php echo $troutheader_From ?>" required>
                                                <option value="<?php echo $troutheader_From ?>" selected><?php echo $troutheader_From ?></option>
                                                <?php while ($data = mysqli_fetch_assoc($result3)) { ?>
                                                    <option value="<?php echo $data['nama_gudang']; ?>"><?php echo $data['nama_gudang']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" name="idx" value=<?php echo $_GET['troutheader_ID']; ?>>
                                        </div>
                                        <div class="lokasi_gudang">
                                            <label>To Location</label>
                                            <select class="custom-select" name="troutheader_To_lokasi" value="<?php echo $troutheader_To_lokasi ?>" required>
                                                <option value="<?php echo $troutheader_To_lokasi ?>" selected><?php echo $troutheader_To_lokasi ?></option>
                                                <?php while ($data = mysqli_fetch_assoc($result4)) { ?>
                                                    <option value="<?php echo $data['nama_gudang']; ?>"><?php echo $data['nama_gudang']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="lokasi_gudang">
                                            <label>Status</label>
                                            <select class="custom-select" name="troutheader_Status" value="<?php echo $troutheader_Status ?>" required>
                                                <option value="<?php echo $troutheader_Status ?>" selected><?php echo $troutheader_Status ?></option>
                                                <option value="posted">Posted</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Description</label>
                                            <textarea name="troutheader_Description" value="<?php echo $troutheader_Description ?>" class="form-control" id="exampleFormControlTextarea1" rows="3"><?php echo $troutheader_Description ?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form_action">
                                            <div class="btn_submit">
                                                <button type="submit" class="btn btn-danger float-left btn_cancel"><a href="trout.php">Cancel</a></button>
                                                <input type="submit" name="update" class="orm-control btn-success btn_simpan float-right" value="Update" />
                                                <div class="bersihkan"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
