<?php
session_start();
error_reporting('E_ALL ^ E_NOTICE');
include_once("../config.php");
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
//$result = mysqli_query($koneksi, "SELECT * FROM trout ORDER BY trout_ID DESC");

$result2 = mysqli_query($koneksi, "SELECT * FROM product_master ORDER BY product_masterID DESC");
$result3 = mysqli_query($koneksi, "SELECT * FROM gudang");
$result4 = mysqli_query($koneksi, "SELECT * FROM gudang");

//$result5 = mysqli_query($koneksi, "SELECT * FROM trout_detail");
//while ($master_data = mysqli_fetch_array($result5)) {
//    $troutheader_Number = $master_data['troutheader_Number'];
//    $troutdetail_Barcode = $master_data['troutdetail_Barcode'];
//}


if (!isset($_SESSION['admin'])) {
    header('location:./../' . $_SESSION['akses']);
    exit();
}

$nama = ( isset($_SESSION['user']) ) ? $_SESSION['user'] : '';
?>


<?php
// Display selected user data based on id
// Getting id from url
$trinheader_ID = $_GET['trinheader_ID'];
//echo json_encode($troutheader_ID); exit();
// Fetech user data based on id
$result = mysqli_query($koneksi, "SELECT * FROM trin_header WHERE trinheader_ID=$trinheader_ID");

while ($master_data = mysqli_fetch_array($result)) {
    $trinheader_UUID = $master_data['trinheader_UUID'];
    //echo json_encode($trinheader_UUID);
    $trinheader_Number = $master_data['trinheader_Number'];
    //echo json_encode($trinheader_Number);
    $troutheader_Number = $master_data['troutheader_Number'];
    //echo json_encode($troutheader_Number);
    $trinheader_From = $master_data['trinheader_From'];
    //echo json_encode($trinheader_From);
    $trinheader_To_lokasi = $master_data['trinheader_To_lokasi'];
    //echo json_encode($trinheader_To_lokasi);
    $trinheader_Status = $master_data['trinheader_Status'];
    //echo json_encode($trinheader_Status);
    $trinheader_Description = $master_data['trinheader_Description'];
    //echo json_encode($trinheader_Description); exit();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Transfer In / Add Barcode Transfer In</h1>
                                            <?php echo $troutheader_Number; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_create">
                            <form method="post" action="" autocomplete="off" class="formadd">
                                <div class="jdltmbh" style="text-align: center; font-size: 17px; text-transform: capitalize; margin-bottom: 20px;">
                                    <span style="font-weight: bold; letter-spacing: 2px; border-bottom: 2px solid #666; text-transform: uppercase; padding-bottom: 2px;"># add barcode #</span>
                                </div>
                                <div class="data_action">
                                    <div class="form-group">
                                        <label>Rack</label>
                                        <input type="text" class="insert_rak" name="trindetail_Rack" placeholder="Rack" style="display: block; width: 100px; padding: 4px 7px; border-radius: 4px; border: 1px solid #ced4da;" />
                                    </div>
                                </div>
                                <div class="data_action">
                                    <div class="form-group">
                                        <input type="text" name="cari" autofocus class="form-control form-control-lg barcode_insert" placeholder="Scan barcode in here">
                                        <input type="submit" name="cari2" value="save" style="display: none;" />
                                    </div>
                                </div>
                                <div class="form_action">
                                    <div class="btn_submit">
                                        <a href="trin.php" class="btn btn-danger">Back</a>
                                        <input type="submit" name="save_trin" class="btn btn-success float-right" value="Simpan" />
                                    </div>
                                </div>
                            </form>
                            <?php
                            if ($_POST['cari2']) {
                                $no = 1; //buat urutan nomer
                                $cari = $_POST['cari'];

                                $sql = "SELECT * FROM trout_detail WHERE troutdetail_Barcode = '$cari' AND troutheader_Number = '$troutheader_Number'";

                                $query = mysqli_query($koneksi, $sql);

                                $sql2 = "SELECT * FROM barcode_trin_temp WHERE trindetail_Barcode = '$cari'";
                                $query2 = mysqli_query($koneksi, $sql2);

                                if ($data = mysqli_fetch_array($query2, MYSQLI_NUM)) {
                                    if ($data[0] > 1) {
                                        echo "<script> alert('Barcode Already exist');
                                                window.location.href='add-barcode-trin.php';  
                                              </script>";
                                        exit();
                                    }
                                } elseif ($data = mysqli_fetch_array($query)) {

                                    $trnheader_UUID = $trinheader_UUID;
                                    //echo json_encode($trnheader_UUID);
                                    $trinheader_number = $trinheader_Number;
                                    //echo json_encode($trinheader_number);
                                    $trindetail_Barcode = $data['troutdetail_Barcode'];
                                    //echo json_encode($trindetail_Barcode);
                                    $trindetail_ProductName = $data['troutdetail_ProductName'];
                                    //echo json_encode($trindetail_ProductName);
                                    $trindetail_From = $trinheader_From;
                                    //echo json_encode($trindetail_From); exit();
                                    //table TEMPORARY = barcode_trin_temp
                                    $querytroutdetail = mysqli_query($koneksi, "INSERT INTO barcode_trin_temp(trinheader_UUID, trinheader_Number, trindetail_Barcode, trindetail_ProductName, trindetail_From) VALUES ('$trnheader_UUID', '$trinheader_number','$trindetail_Barcode','$trindetail_ProductName', '$trindetail_From')");
                                } else {
                                    echo "<table style='width: 100%;'>";
                                    echo "<tr><td colspan='4'><center><h6>Barcode ( <span style='color: red;'>$cari</span> ) Not found...!!!</h6></center></td></tr>";
                                    echo "</table>";
                                    echo '<noscript>';
                                    echo '<meta http-equiv="refresh" content="0;url="add-barcode-trin.php" />';
                                    echo '</noscript>';
                                    exit();

//                                    echo '<script type="text/javascript">';
//                                    echo 'window.location.href="' . $url . '";';
//                                    echo '</script>';
//                                    echo '<noscript>';
//                                    echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
//                                    echo '</noscript>';
//                                    exit;
//
//                                    echo "<script> alert('Barcode Already exist');
//                                                window.location.href='add-barcode-trin.php';  
//                                          </script>";
//                                    exit();
                                }
                            }
                            ?>

                            <?php
                            if ($_POST['save_trin']) {
                                //$no = 1; //buat urutan nomer
                                $trindetail_Rack = $_POST['trindetail_Rack'];
                                //echo json_encode($trindetail_Rack); exit();
                                $update_barcode_temp = mysqli_query($koneksi, "UPDATE barcode_trin_temp SET trindetail_Rack='$trindetail_Rack'");
                                //echo json_encode($update_barcode_temp); exit();

                                if ($update_barcode_temp) {
                                    $copytable = mysqli_query($koneksi, "INSERT INTO trin_detail(trinheader_UUID, trinheader_Number, trindetail_Barcode, trindetail_ProductName, trindetail_From, trindetail_Rack) SELECT trinheader_UUID, trinheader_Number, trindetail_Barcode, trindetail_ProductName, trindetail_From, trindetail_Rack FROM barcode_trin_temp");
                                    //echo json_encode($copytable); exit();
                                }

                                if ($copytable) {
                                    $deleteallfile = mysqli_query($koneksi, "DELETE FROM barcode_trin_temp");
                                    //echo json_encode($deleteallfile); exit();
                                }
                            }
                            ?>
                        </div>

                        <div class="data_in">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>TRIN Number</th>
                                        <th>Product Name</th>
                                        <th>Product Barcode</th>
                                        <th>From Location</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    //$sql = "SELECT trh.trinheader_Number, trindetail_ProductName, trindetail_Barcode, trindetail_From FROM trin_header trh JOIN trin_detail trd ON trh.trinheader_UUID = trd.trinheader_UUID WHERE trd.trinheader_Number = '$trinheader_Number' ORDER BY trindetail_ID DESC";
                                    $sql = "select * from barcode_trin_temp order by id DESC";
                                    $result_set = mysqli_query($koneksi, $sql);

                                    while ($row = mysqli_fetch_array($result_set)) {
                                        ?>
                                        <tr>	
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $row['trinheader_Number'] ?></td>
                                            <td><?php echo $row['trindetail_ProductName'] ?></td>
                                            <td><?php echo $row['trindetail_Barcode'] ?></td>
                                            <td><?php echo $row['trindetail_From'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
