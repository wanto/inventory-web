<!DOCTYPE html>
<html>
    <?php
    error_reporting(false);
    session_start();
    include_once("../config.php");
    ?>
    <head>
        <title>Tugas 2</title>
    </head>
    <body>
        <div align="center" style="font-family:arial;font-size:30px;">
            Program Tambah Data tanpa Database Menggunakan Session</div>
        </br>

        <form method="post">
            <table>
                <tr>
                    <td>Masukkan Nama</td>
                    <td>: <input type="text" name="nama"></td>
                </tr>
                <tr>
                    <td>Masukkan Alamat</td>
                    <td>: <input type="text" name="alamat"></td>
                </tr>
                <tr>
                    <td><input type="submit" name="kirim" value="Kirim"></td>
                </tr>
            </table>
        </form>
        <?php
        // Ambil data dari session
        if (isset($_SESSION['tmpnama'])) {
            $tmpnama = $_SESSION['tmpnama'];
        }
        if (isset($_SESSION['tmpalamat'])) {
            $tmpalamat = $_SESSION['tmpalamat'];
        }
        // End ambil data dari session
        // Tambahkan array (hasil dari data session tadi) dengan data inputan yang baru
        $tmpnama[] = $_POST['nama'];
        $tmpalamat[] = $_POST['alamat'];
        // End script tambah ke array
        // Simpan data array yang baru ke session
        $_SESSION['tmpnama'] = $tmpnama;
        $_SESSION['tmpalamat'] = $tmpalamat;
        // End script simpan ke session
        
        $masuk = mysqli_query($koneksi, "INSERT INTO test(nama,alamat) VALUES ('$tmpnama','$tmpalamat)");
        echo json_encode($masuk); exit();
        ?>
        <br>
        <table>
            <tr>
                <td>No.</td>
                <td>Nama</td>
                <td>Alamat</td>
            </tr>
            <?php
            
            for ($i = 0; $i < count($tmpnama); $i++) {
                echo "<tr><td>";
                echo 1 + $i . "</td><td>" . $tmpnama[$i] . "</td><td>" . $tmpalamat[$i] . "</td></tr>";
            }
// End script urai array
            session_destroy();
            ?>
        </table>
    </body>
</html>

<?php
// Ambil data dari session
if ($_POST['savedetail']) {
    if (isset($_SESSION['lokasi'])) {
        $lokasi = $_SESSION['lokasi'];
    }
    if (isset($_SESSION['barcode'])) {
        $barcode = $_SESSION['barcode'];
    }
    // End ambil data dari session
    // Tambahkan array (hasil dari data session tadi) dengan data inputan yang baru
    $lokasi[] = $_POST['troutdetail_to_lokasi'];
    $barcode[] = $_POST['troutdetail_Barcode'];
    // End script tambah ke array
    // Simpan data array yang baru ke session
    $_SESSION['lokasi'] = $lokasi;
    $_SESSION['barcode'] = $barcode;
    // End script simpan ke session
}
?>


<form method="post" action="" autocomplete="off" class="myform" style="margin-top: 100px;">
    <div class="row">
        <div class="col-md-6">
            <div class="lokasi_gudang">
                <label>From Location</label>
                <select class="custom-select" name="troutdetail_to_lokasi">
                    <option value="" selected>Select Location</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
            <div class="lokasi_gudang">
                <label>To Location</label>
                <select class="custom-select" name="troutdetail_to_lokasi">
                    <option value="" selected>Select Location</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="lokasi_gudang">
                <label>Status</label>
                <select class="custom-select" name="troutdetail_to_lokasi">
                    <option value="" selected>Select Status</option>
                    <option value="posted">Posted</option>
                    <option value="save">Save</option>
                </select>
            </div>
            
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Description</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form_action">
                <div class="btn_submit">
                    <button type="submit" class="btn btn-danger float-left btn_cancel"><a href="trout.php">Cancel</a></button>
                    <input type="submit" name="submit" class="orm-control btn-success btn_simpan float-right" value="Submit" />
                    <div class="bersihkan"></div>
                </div>
            </div>
        </div>
    </div>
</form>