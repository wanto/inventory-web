<?php
session_start();
include_once("../config.php");
$result = mysqli_query($koneksi, "SELECT * FROM users ORDER BY user_ID DESC");

if( !isset($_SESSION['user']) )
{
	header('location:./../'.$_SESSION['akses']);
	exit();
}else{
	$nama = $_SESSION['user'];
}


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->

    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Transfer Out</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="create_in">
                            <button type="button" class="btn btn-success"><a href="create-trout.php">create new</a></button>
                        </div>

                        <div class="data_in">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Trout Number</th>
                                        <th>From Location</th>
                                        <th>To Location</th>
                                        <th>Status</th>
                                        <th>Description</th>
                                        <th>Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    $sql = "SELECT * FROM trout_header ORDER BY troutheader_ID DESC";
                                    $result_set = mysqli_query($koneksi, $sql);

                                    while ($row = mysqli_fetch_array($result_set)) {

                                        echo "<tr>";
                                        echo "<td>" . $no++ . "</td>";
                                        echo "<td>" . $row['troutheader_Number'] . "</td>";
                                        echo "<td>" . $row['troutheader_From'] . "</td>";
                                        echo "<td>" . $row['troutheader_To_lokasi'] . "</td>";
                                        echo "<td>" . $row['troutheader_Status'] . "</td>";
                                        echo "<td>" . $row['troutheader_Description'] . "</td>";
                                        echo "<td>" . $row['troutheader_created'] . "</td>";
                                        if($row['troutheader_Status'] == 'posted'){
                                            echo "<td align='center'><a href='add-barcode-trout.php?troutheader_ID=$row[troutheader_ID]' title='disable' style='text-decoration: none; cursor: not-allowed; color:currentColor ; pointer-events: none;'><i class='fa fa-barcode'></i></a> | <a href='edit-trout.php?troutheader_ID=$row[troutheader_ID]' title='Edit Data' style='text-decoration: none;'><i class='fa fa-pencil'></i></a> | <a href='view-trout.php?troutheader_ID=$row[troutheader_ID]' title='Lihat Data' style='text-decoration: none;'><i class='fa fa-eye'></i></a></td>";
                                        } else {
                                            echo "<td align='center'><a href='add-barcode-trout.php?troutheader_ID=$row[troutheader_ID]' title='Add Barcode' style='text-decoration: none;'><i class='fa fa-barcode'></i></a> | <a href='edit-trout.php?troutheader_ID=$row[troutheader_ID]' title='Edit Data' style='text-decoration: none;'><i class='fa fa-pencil'></i></a> | <a href='view-trout.php?troutheader_ID=$row[troutheader_ID]' title='Lihat Data' style='text-decoration: none;'><i class='fa fa-eye'></i></a></td>";
                                        }
                                        
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
