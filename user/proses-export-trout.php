<?php

// Load file koneksi.php
include_once("../config.php");

// Load plugin PHPExcel nya
require_once 'PHPExcel/PHPExcel.php';

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel
$csv->getProperties()->setCreator('My Notes Code')
        ->setLastModifiedBy('My Notes Code')
        ->setTitle("Data TROUT")
        ->setSubject("Product")
        ->setDescription("report TROUT")
        ->setKeywords("Report TROUT");

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "TROUT Number");
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Product Barcode");
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Product Name");
$csv->setActiveSheetIndex(0)->setCellValue('E1', "To");

$troutheader_ID = $_GET['troutheader_ID'];

$sqlb = mysqli_query($koneksi, "SELECT * FROM trout_header WHERE troutheader_ID=$troutheader_ID");

while ($rowb = mysqli_fetch_array($sqlb)) {

    $troutheader_Number = $rowb['troutheader_Number'];
    //echo json_encode($trinheader_Number); exit();
}

//$trinheader_Number = 
// Buat query untuk menampilkan semua data siswa
//$sql = mysqli_query($koneksi, "SELECT * FROM trin_detail");
//$sql = mysqli_query($koneksi, "SELECT * FROM trin_header WHERE trinheader_ID=$trinheader_ID");
$sql = mysqli_query($koneksi, "SELECT trh.troutheader_Number, troutdetail_ProductName, troutdetail_Barcode, troutdetail_To_lokasi FROM trout_header trh JOIN trout_detail trd ON trh.troutheader_UUID = trd.troutheader_UUID WHERE trd.troutheader_Number = '$troutheader_Number' ORDER BY troutdetail_ID DESC");
//$sql = mysqli_query($koneksi, "SELECT trh.trinheader_Number, trd.trindetail_ProductName, trd.trindetail_Barcode, trd.trindetail_From, trd.trindetail_Rack FROM trin_header trh JOIN trin_detail trd ON trh.trinheader_UUID = trd.trinheader_UUID WHERE trd.trinheader_Number = '$trinheader_Number' ORDER BY trindetail_ID DESC");

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
while ($data = mysqli_fetch_array($sql)) { // Ambil semua data dari hasil eksekusi $sql
    $csv->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data['troutheader_Number']);
    $csv->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data['troutdetail_Barcode']);
    $csv->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data['troutdetail_ProductName']);
    $csv->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data['troutdetail_To_lokasi']);

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
$csv->getActiveSheet(0)->setTitle("report all TROUT data");
$csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="TROUT-Report.csv"'); // Set nama file excel nya
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
