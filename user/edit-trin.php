<?php
session_start();
error_reporting('E_ALL ^ E_NOTICE');
include_once("../config.php");
$conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
$result = mysqli_query($koneksi, "SELECT * FROM trout ORDER BY trout_ID DESC");
$result2 = mysqli_query($koneksi, "SELECT * FROM product_master ORDER BY product_masterID DESC");
$result3 = mysqli_query($koneksi, "SELECT * FROM gudang");
$result4 = mysqli_query($koneksi, "SELECT * FROM gudang");



if( !isset($_SESSION['user']) )
{
	header('location:./../'.$_SESSION['akses']);
	exit();
}else{
	$nama = $_SESSION['user'];
}

?>

<?php
// include database connection file
include_once("../config.php");

// Check if form is submitted for user update, then redirect to homepage after update
if (isset($_POST['update'])) {
    $id = $_POST['idx'];
    $trinheader_From = $_POST['trinheader_From'];
    //echo json_encode($user_nama);
    $trinheader_To_lokasi = $_POST['trinheader_To_lokasi'];
    //echo json_encode($user_alamat);
    $trinheader_Status = $_POST['trinheader_Status'];
    //echo json_encode($user_telepon);
    $trinheader_Description = $_POST['trinheader_Description'];

    // update user data
    $result = mysqli_query($koneksi, "UPDATE trin_header SET trinheader_From='$trinheader_From',trinheader_To_lokasi='$trinheader_To_lokasi',trinheader_Status='$trinheader_Status',trinheader_Description='$trinheader_Description' WHERE trinheader_ID=$id");
    //echo json_encode($result); exit();
    // Redirect to homepage to display updated user in list
    header("Location: trin.php");
}
?>

<?php
// Display selected user data based on id
// Getting id from url
$trinheader_ID = $_GET['trinheader_ID'];
//echo json_encode($troutheader_ID); exit();
// Fetech user data based on id
$result = mysqli_query($koneksi, "SELECT * FROM trin_header WHERE trinheader_ID=$trinheader_ID");

while ($master_data = mysqli_fetch_array($result)) {
    $trinheader_UUID = $master_data['trinheader_UUID'];
    $trinheader_Number = $master_data['trinheader_Number'];
    $troutheader_Number = $master_data['troutheader_Number'];
    $trinheader_From = $master_data['trinheader_From'];
    $trinheader_To_lokasi = $master_data['trinheader_To_lokasi'];
    $trinheader_Status = $master_data['trinheader_Status'];
    $trinheader_Description = $master_data['trinheader_Description'];
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventory</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="../assets/css/simple-sidebar.css" rel="stylesheet">
        <link href="../assets/css/style.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="../assets/DataTables/css/dataTables.bootstrap4.min.css">

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="../assets/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../assets/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <!-- Menu Toggle Script -->
        <script type="text/javascript" src="../assets/js/jquery.autocomplete.min.js"></script>
        <style>
            
            .autocomplete-suggestions {
                border: 1px solid #ced4da;
                background: #FFF;
                overflow: auto;
                border-radius: 3px;
            }
            .autocomplete-suggestion {
                padding: 5px 13px;
                white-space: nowrap;
                overflow: hidden;
            }
            .autocomplete-selected {
                background: #F0F0F0;
            }
            .autocomplete-suggestions strong {
                font-weight: normal;
                color: #3399FF;
            }
            .autocomplete-group {
                padding: 5px 13px;
            }
            .autocomplete-group strong {
                display: block;
                border-bottom: 1px solid #000;
            }

        </style>
    </head>

    <body>
        <div class="d-flex" id="wrapper">
            <?php include('sidemenu.php'); ?>

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <?php include('navbar.php'); ?>

                <div class="container-fluid">
                    <div class="content">
                        <div class="breadcrumbs">
                            <div class="row">
                                <div class="col">
                                    <div class="page-header float-left">
                                        <div class="page-title">
                                            <h1>Transfer Out / Create Transfer In</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_create">


                            <form method="post" action="" autocomplete="off" class="myform">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="lokasi_gudang">
                                            <label>Trout</label>
                                            <input type="text" id="trout" class="form-control" name="trout" value="<?php echo $troutheader_Number ?>" disabled />
                                            <input type="hidden" name="idx" value=<?php echo $_GET['trinheader_ID']; ?>>
                                        </div>
                                        <div class="lokasi_gudang">
                                            <label>From Location</label>
                                            <select class="custom-select" name="trinheader_From" value="<?php echo $trinheader_From ?>" required>
                                                <option value="<?php echo $trinheader_From ?>" selected><?php echo $trinheader_From ?></option>
                                                <?php while ($data = mysqli_fetch_assoc($result3)) { ?>
                                                    <option value="<?php echo $data['nama_gudang']; ?>"><?php echo $data['nama_gudang']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="lokasi_gudang">
                                            <label>To Location</label>
                                            <select class="custom-select" name="trinheader_To_lokasi" value="<?php echo $trinheader_To_lokasi ?>" required>
                                                <option value="<?php echo $trinheader_To_lokasi ?>" selected><?php echo $trinheader_To_lokasi ?></option>
                                                <?php while ($data = mysqli_fetch_assoc($result4)) { ?>
                                                    <option value="<?php echo $data['nama_gudang']; ?>"><?php echo $data['nama_gudang']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="lokasi_gudang">
                                            <label>Status</label>
                                            <select class="custom-select" name="trinheader_Status" value="<?php echo $trinheader_Status ?>" required>
                                                <option value="<?php echo $trinheader_Status ?>" selected><?php echo $trinheader_Status ?></option>
                                                <option value="posted">Posted</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Description</label>
                                            <textarea name="trinheader_Description" value="<?php echo $trinheader_Description ?>" class="form-control" id="exampleFormControlTextarea1" rows="3"><?php echo $trinheader_Description ?></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form_action">
                                            <div class="btn_submit">
                                                <button type="submit" class="btn btn-danger float-left btn_cancel"><a href="trin.php">Cancel</a></button>
                                                <input type="submit" name="update" class="orm-control btn-success btn_simpan float-right" value="Update" />
                                                <div class="bersihkan"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Menu Toggle Script -->

        <script type="text/javascript">
            $(document).ready(function () {
                // Selector input yang akan menampilkan autocomplete.
                $("#trout").autocomplete({
                    serviceUrl: "source.php", // Kode php untuk prosesing data.
                    dataType: "JSON", // Tipe data JSON.
                    onSelect: function (suggestion) {
                        $("#buah").val("" + suggestion.trout);
                    }
                });
            })
        </script>

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    colReorder: true
                });
            });
        </script>
    </body>
</html>
